import boto3
import tempfile
import os
import io
from utils import Util


class AWSS3(object):
    
    def __init__(self):
        self.s3 = boto3.client("s3",region_name='ap-southeast-1')
        
    def fetch_s3_object(self, bucket, key, region_name="ap-southeast-1"):
        try:
            s3 = boto3.resource('s3', region_name='ap-southeast-1')

            obj = s3.Object(bucket, key)
            s = obj.get()['Body'].read()
            # print(s)
            
            
            
            # print(bucket)
            # key = "facility-01/study-01/images.jpeg"
            # print(key)
            # s3 = boto3.resource('s3', region_name=region_name)
            # bucket = s3.Bucket(bucket)
            # object = bucket.Object(key)
            # print(key)
            # print(object)
            # print(key)
            # key = "2222-3333-4444-5555/TG18-pQeC/TG18-PQC-1k-01 - Copy.dcm"
            file_dir = Util.get_full_dir(key)
            os.makedirs("/tmp/%s" % file_dir, exist_ok=True)
            # tmp = tempfile.NamedTemporaryFile()
            tmp_file = '/tmp/%s' % key
            # print(tmp_file)
            # # d = object.get()['Body'].read().decode('utf-8')
            # # print(d)
            # file_stream = io.BytesIO()
            # object.download_fileobj(file_stream)
            # print(file_stream.read())
            # print("Done!")
            # return "Hmm"
            print(type(s))
            print(len(s))
            with open(tmp_file, 'wb') as f:
                # self.s3.download_fileobj(bucket, key, f)
                f.write(s)
                pass
            print(tmp_file)
            
            # with open('/tmp/%s' % key, 'wb') as data:
            #     bucket.download_fileobj(key, data)
            # print("Done")
            
            # fn='/tmp/%s' % key
            # fp=open(fn,'w')
            # response = self.s3.get_object(Bucket=bucket,Key=key.encode())
            # contents = response['Body'].read()
            # print(contents)
            # fp.write(contents)
            # fp.close()
            
            # response = self.s3.get_object(
            #     Bucket=bucket,
            #     Key=key
            # )
            
            # print(response)
            
            print("Done!")
            return tmp_file
            
        except Exception as exp:
            print(str(exp))
            pass