import boto3


class AWSS3(object):
    
    def __init__(self):
        self.s3 = boto3.client("s3")
        
    def fetch_s3_object(self, bucket, key):
        try:
            obj = self.s3.get_object(Bucket=bucket, Key=key)
            print(obj)
            return obj['Body'].read()
        except Exception as exp:
            print(str(exp))
            pass