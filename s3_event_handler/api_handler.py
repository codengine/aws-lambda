from botocore.vendored import requests
import json
import os


class APIHandler(object):

    @classmethod
    def post_drive_changes_to_cloud(cls, url, data, files = {}, method='POST'):
        try:
            if method == 'POST':
                response = requests.post(url, files=files, json=data)
                if not files:
                    json_response = json.loads(response.content.decode(encoding='utf-8'))
                    return json_response['guid']
                else:
                    return response.status_code == 200
            elif method == 'PUT':
                response = requests.put(url, json=data)
                if response.status_code == 200:
                    return True
        except Exception as exp:
            print(str(exp))

    @classmethod
    def create_study(cls, request_data):
        # data = {
        #     "facility_guid":"bd98cb78-149f-43d3-9602-7fcb693363ea"
        # }
        url = 'http://cloud.alemhealth.com:80/api/orders/ah-connect/?secret_key=AH-cfb618dd24b34810ae56b07dc8bb7e2d'
        return cls.post_drive_changes_to_cloud(url=url, data=request_data)

    @classmethod
    def update_study(cls, facility_id, request_data):
        url = 'http://cloud.alemhealth.com:80/api/orders/ah-connect/%s?secret_key=AH-cfb618dd24b34810ae56b07dc8bb7e2d' % facility_id
        return cls.post_drive_changes_to_cloud(url=url, data=request_data, method="PUT")

    @classmethod
    def upload_file(cls, facility_id, file_path):
        url = 'http://cloud.alemhealth.com:80/api/dicoms/ah-connect/%s?secret_key=AH-cfb618dd24b34810ae56b07dc8bb7e2d' % facility_id
        file = open(file_path, 'rb')
        files = {
            'file': file
        }
        uploaded = cls.post_drive_changes_to_cloud(url=url, files=files, data={})
        if uploaded:
            try:
                file.close()
                os.remove(file_path)
            except Exception as exp:
                pass
        return uploaded