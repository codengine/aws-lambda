import json
import boto3
from dbutil import DBUtil
from api_handler import APIHandler
from aws_s3 import AWSS3
from utils import Util


def lambda_handler(event, context):
    db = DBUtil()
    s3 = AWSS3()
    bucket_name = "alemhealth-windows-upload-test-bucket"
    
    if "DB" in event:
        # Create the DB
        try:
            print("Creating DB...")
            db.create_all_table()
            # db.insert_study_item(bucket_name='test_bucket', study_name='test_study', item_name='test_item')
            print("DB Created.")
            return "Created"
        except Exception as exp:
            print(str(exp))
            return "Creation Failed"
    
    # TODO implement
    print(event)
    
    records = event["Records"]
    for record in records:
        bucket = record["s3"]["bucket"]["name"]
        s3_object = record["s3"]["object"]["key"]
        aws_resource = Util.detect_if_resource(s3_object)
        facility_guid, study_name, resource_name = Util.extract_resource_name(s3_object)
        if facility_guid and study_name:
            inserted = db.insert_study_if_not_exists(bucket, facility_guid, study_name)
            if aws_resource:
                db.insert_study_item(bucket_name=bucket, facility_guid=facility_guid, study_name=study_name, item_name=s3_object)
                return "Study Created and Also resource inserted"
            else:
                return "Only study created."
        else:
            return "Neither study created nor resource inserted. Study name extracted None."
